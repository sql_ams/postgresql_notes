# CREATING DBs AND TABLES
---
### DATA TYPES
+ Boolean
  + Inserted data will be converted into the boolean value e.g. **1, yes, y, t, true** are converted to **true** and **0, no, n, f, false** are converted to false.
  + Selected data from a boolean column, will be display by postgreSQL **like t for true, f for false and space for null**
+ Character
  - Single char: **char**
  - Fixed length character strings: **char(n)**. If the inserted string is shorter postgreSQL will pad spaces, if the string is longer postgreSQL will issue an error.
  - Variable-length character strings: **varchar(n)**. PostgreSQL does not pad spaces when the string is shorter than the length.
+ Number
  - _Integers_
    - **smallint**: 2-byte signed int range of (-32768, 32767)
    - **int**: 4-byte integer range of (-214783648, 214783647)
    - **serial**: Populates vaule into column automatically, similar to AUTO_INCREMENT in other DBMSs.
  - _Floating-point numbers_
    - **float(n)**: precision at least **n**, up to a maximum of **8 bytes**.
    - **real** or **float8**: double-precision 8-byte floating number
    - **numeric** or **numeric(p,s)**: Real number with **p** digits with **s** number after the decimal point. The **numeric(p,)** is the exact number
+ Temporal
  - **date**
  - **time**
  - **timestamp**: stores date and time
  - **interval**: stores difference in timestamps
  - **timestampz**: store both timestamp and timezone data
+ Special Types
+ Array
---
### CREATE

##### Example
```sql
CREATE TABLE copy (LIKE original);
```
---
### INSERT

##### Example
```sql
INSERT INTO link(url, name)
VALUES ('www.google.com', 'Google');
INSERT INTO link_copy
SELECT * FROM link;
```
---
### UPDATE

##### Example
```sql
UPDATE link
SET name = 'Bing'
WHERE id = 5;
UPDATE link
SET description = 'New Desc'
WHERE id = 1
RETURNING id, url, name, description;
```
---
### DELETE

##### Example
```sql
DELETE FROM link
WHERE id = 5
RETURNING *;
```
---
### ALTER TABLE

##### Example
```sql
ALTER TABLE link ADD COLUMN active boolean;
ALTER TABLE link DROP COLUMN active;
ALTER TABLE link RENAME COLUMN title TO new_name;
ALTER TABLE link RENAME TO new_table_name;
```
---
### DROP TABLE

##### Example
```sql
DROP TABLE [IF_EXISTS] test_two;
DROP TABLE [IF_EXISTS] test_two RESTRICT;
DROP TABLE [IF_EXISTS] test_two CASCADE;
```
---
### CHECK CONSTRAINT

+ Allows to specify if a value in a column must meet a specific requirement. If the values of the column pass the check it will insert or update the values.

##### Example
```sql
CREATE TABLE new_users(
  id serial PRIMARY KEY,
  first_name VARCHAR(50),
  birth_date DATE CHECK(birth_date > '1900-01-01')
);
CREATE TABLE checktest(
  sales integer CONSTRAINT positive_sales CHECK(sales > 0));
)
```
---
### VIEW

+ Is a DB object that is of a stored query
+ Can be accessible as a virtual table
+ Logical table that represents data of one or more underlying tables through a SELECT statement.

##### Example
```sql
CREATE VIEW customer_info AS
SELECT first_name, last_name, email, address, phone
FROM customer
JOIN address
ON customer.address_id = address.address_id;
SELECT * FROM customer_info;
```
---
