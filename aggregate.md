# AGGREGATE AND GROUPING
---
### MIN MAX AVG SUM
+ They take data and combine it to a single value.

##### Example
```sql
SELECT AVG(amount) FROM payment;
SELECT ROUND(AVG(amount), 2) FROM payment;
```
---
### GROUP BY
+ Divides the rows returned from the **SELECT** statement into groups. For each group an aggregate function can be applied.

##### Example
```sql
SELECT customer_id, SUM(amount)
FROM payment
GROUP BY customer_id
ORDER bY SUM(amount);
```
### HAVING
+ Filters group rows that do not satisfy a specified condition. Similar to **WHERE** but with grouping. The difference is that **HAVING** sets the condition for **group rows** **_after_** the **GROUP BY** clause applies, while **WHERE** does it for individual rows **_before_** **GROUP BY** applies.

##### Example
```sql
SELECT customer_id, SUM(amount)
FROM payment
GROUP BY customer_id
HAVING SUM(amount) > 200;
SELECT rating, AVG(rental_rate)
FROM film
WHERE rating IN ('R', 'G', 'PG')
GROUP BY rating
HAVING AVG(rental_rate) < 3;
```
