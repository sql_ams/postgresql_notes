# JOINS
---
### OVERVIEW
![join overview](https://lukaseder.files.wordpress.com/2015/10/venn.png?w=662)

---
### AS
+ Allows to rename columns or table selections wit an alias.

##### Example
```sql
SELECT payment_id AS payment_info FROM payment;
```
---
### INNER JOIN
+ Produces only the set of records that match in both Table A and Table B.

##### Example
```sql
SELECT
customer.customer_id,
customer.name,
payment.amount,
payment.payment_date
FROM customer
INNER JOIN payment ON payment.customer_id = customer.customer_id;

SELECT title, COUNT(title) AS copies_at_store1
FROM inventory
INNER JOIN film ON inventory.film_id = film.film_id
WHERE store_id = 1
GROUP BY title;
```
---
### FULL OUTER JOIN
+ Produces the set of all records in A and B, with matching records from both sides where available. If there is no match, the missing side will contain null

##### Example
```sql
SELECT * FROM TableA
FULL OUTER JOIN TableB
ON TableA.name = TableB.name;
```
---
### FULL OUTER JOIN with where
+ Performs the same fullouter join, then excludes the records we don't want from both sides via a where clause.

##### Example
```sql
SELECT * FROM TableA
FULL OUTER JOIN TableB
ON TableA.name = TableB.name
WHERE TableA.id IS null
OR TableB.id IS null;
```
---
### LEFT/RIGHT OUTER JOIN
+ Produces a complete set of records from A, with the mathicn records (where available) in Table B. If there is no match, the right side will contain null.

##### Example
```sql
SELECT * FROM TableA
LEFT OUTER JOIN TableB
ON TableA.name = TableB.name;
```
---
### LEFT/RIGHT OUTER JOIN with WHERE
+ Performs the same outer join, then excludes the records we don't want form the right side via a where clause.

##### Example
```sql
SELECT * FROM TableA
LEFT OUTER JOIN TableB
ON TableA.name = TableB.name
WHERE TableB.id IS null;
```
---
### UNION
+ Combines result sets of two or more SELECT statements into a single result set. Both queries must return the same number of columns and the corresponding columns in the queries must have compatible data types. **UNION** removes all duplicate rows unless the **UNION ALL** is used.

##### Example
```sql
SELECT * FROM TableA
LEFT OUTER JOIN TableB
ON TableA.name = TableB.name
WHERE TableB.id IS null;
```
---
