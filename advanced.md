# ADVANCED
---
### TIMESTAMPS AND EXTRACTS FUNCTIONS

[Date functions](https://www.postgresql.org/docs/current/static/functions-datetime.html)

---
### MATHEMATICAL FUNCTIONS

[Math func](https://www.postgresql.org/docs/current/static/functions-math.html)

---
### STRING FUNCTIONS AND OPERATORS

[String func](https://www.postgresql.org/docs/current/static/functions-string.html)

---
### SUBQUERY
+ Allows to use multiple SELECT statements, where we basically have a query within a query.

##### Example
```sql
SELECT title, rental_rate
FROM film
WHERE rental_rate > (SELECT AVG(rental_rate) FROM film);
```
---
### SELF JOIN
+

##### Example
```sql
SELECT a.first_name, a.last_name, b.first_name, a.last_name
FROM customer AS a, customer AS b
WHERE a.first_name = b.last_name;
```
```sql
SELECT a.first_name, a.last_name, b.first_name, a.last_name
FROM customer AS a
JOIN customer AS b
ON a.first_name = b.last_name;
```
---
