# FUNDAMENTALS
---
### SELECT
+ Don't use **\*** if possible, it makes the DB work harder and increases the traffic between the DB and the application. Specify the needed column names.

##### Example
```sql
SELECT first_name, last_name FROM actor;
```
---
### SELECT DISTINCT
+ Columns in a table may contain duplicate values; sometimes it is necessary to list only distinct values. The **DISTINCT** keyword can be used for that.

##### Example
```sql
SELECT DISTINCT rental_rate FROM film;
```
---
### SELECT WHERE
+ Is used to filter particular rows

##### Example
```sql
SELECT email
  FROM customer
  WHERE first_name = 'Jared';
```
---
### COUNT
+ Returns the number of input rows that match a specific condition of a query. Count does not consider **NULL** values in the column. Can be used with **DISTINCT**.

##### Example
```sql
SELECT COUNT(amount) FROM payment;
SELECT COUNT(DISTINCT(amount)) FROM payment;
```
---
### LIMIT
+ Allows to limit the number of rows returned.

##### Example
```sql
SELECT * FROM customer LIMIT 5;
```
---
### ORDER BY
+ Allows to sort the result set. Without clause it will default to **ASC**.

##### Example
```sql
SELECT first_name, last_name
FROM customer
ORDER BY first_name ASC;
SELECT first_name, last_name
FROM customer
ORDER BY first_name ASC, last_name DESC;
```
---
### BETWEEN
+ Match a value against a range of values

##### Example
```sql
SELECT customer_id, amount
FROM payment
WHERE amount BETWEEN 8 AND 9;
SELECT customer_id, amount
FROM payment
WHERE amount NOT BETWEEN 8 AND 9;
```
---
### IN
+ Use **IN** with **WHERE** to check if a value matches a value in a list of values.

##### Example
```sql
SELECT customer_id, rental_id
FROM rental
WHERE customer_id IN (1, 2);
```
---
### LIKE
+ Allows to search for a certain pattern. If you don't want case sensitivity use **ILIKE** or **LOWER()**.

##### Example
```sql
SELECT first_name, last_name
FROM customer
WHERE first_name LIKE 'Jen%';
```
